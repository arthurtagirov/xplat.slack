#!/bin/bash

# Slack incoming web-hook URL and user name
url='https://hooks.slack.com/services/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'
username='Zabbix'

## Values received by this script:
# To = $1 (Slack channel or user to send the message to, specified in the Zabbix web interface; "@username" or "#channel")
# Subject = $2 (usually either PROBLEM or RECOVERY)
# Message = $3 (whatever message the Zabbix action sends, preferably something like "Zabbix server is unreachable for 5 minutes - Zabbix server (127.0.0.1)")

# Get the Slack channel or user ($1) and Zabbix subject ($2 - hopefully either PROBLEM or RECOVERY)
to="$1"
subject="$2"
body="$3"

# color of attachment
if [[ "$subject" =~ 'OK' ]]; then
	color='good'
elif [[ "$subject" =~ 'PROBLEM' ]]; then
	color='danger'
else
	color='warning'
fi

if [[ "$subject" =~ 'xplat-rabbit1' ]]; then
	emoji=':rabbit:'
	author='RabbitMQ'
	author_link='http://xplat-rabbit1:15672/#/'
	if [[ "$subject" =~ 'reconciliations/' ]]; then
		to="#reconciliation"
	elif [[ "$subject" =~ 'Scheduler/' ]]; then
		to="#scheduler"
	fi
else
	emoji=':ghost:'
	author='Zabbix'
	author_link='http://zabbix.walletone.local/zabbix/tr_status.php'
fi

attachment="{ \"fallback\": \"${body}\", \"color\": \"${color}\", \"author_name\": \"${author}\", \"author_link\": \"${author_link}\", \"text\": \"${body}\" }"
# Build our JSON payload and send it as a POST request to the Slack incoming web-hook URL
payload="payload={\"channel\": \"${to}\", \"username\": \"${username}\", \"icon_emoji\": \"${emoji}\", \"attachments\": [ ${attachment} ]}"
curl -m 5 --data-urlencode "${payload}" $url